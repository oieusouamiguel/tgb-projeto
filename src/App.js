import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Home from './pages/Home'
import Historia from './pages/Historia'
import Loja from './pages/Loja'
import Contato from './pages/Contato'
import Error from './pages/Error'

class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/historia" component={Historia} />
            <Route path="/loja" component={Loja} />
            <Route path="/contato" component={Contato} />
            <Route component={Error} />
          </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
